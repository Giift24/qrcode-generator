import os
import qrcode

# Prompt the user to enter the data for the QR code
while True:
    data = input('Enter the data for the QR code: ').strip()
    if data:
        break
    else:
        print('Please enter valid data.')

# Create the QR code object
qr = qrcode.QRCode(
    version=1,
    error_correction=qrcode.constants.ERROR_CORRECT_L,
    box_size=10,
    border=4,
)

# Add the data to the QR code object
qr.add_data(data)
qr.make(fit=True)

# Create an image from the QR code object
img = qr.make_image(fill_color='black', back_color='white')

# Save the image to a file
while True:
    file_name = input('Enter the file name for the QR code image: ').strip()
    if file_name:
        try:
            # Get the path to the "Pictures" directory based on the OS
            if os.name == 'nt':  # Windows
                pictures_dir = os.path.join(os.path.expanduser('~'), 'Pictures')
            elif os.name == 'posix':  # Linux/Mac
                pictures_dir = os.path.join(os.path.expanduser('~'), 'Pictures')
            elif os.name == 'android':  # Android
                pictures_dir = '/storage/emulated/0/Pictures'
            else:
                raise OSError('Unsupported operating system.')
            
            file_path = os.path.join(pictures_dir, file_name + '.png')  # Append ".png" to the file name
            img.save(file_path)
            print('QR code was successfully saved!')
            break
        except Exception as e:
            print(f'Error saving QR code: {e}')
    else:
        print('Please enter a valid file name.')
